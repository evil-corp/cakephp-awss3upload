<?php
use Cake\Core\Configure;

Configure::load('EvilCorp/AwsS3Upload.awss3');
collection((array)Configure::read('AwsS3Upload.config'))->each(function ($file) {
    Configure::load($file);
});
